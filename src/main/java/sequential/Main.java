package sequential;

import common.Config;
import common.SteepestDescentMethod;
import common.TimeMetric;

public class Main {

    public static void main(String... args) {
        TimeMetric.runTimed(() -> {
            double minValue = new SteepestDescentMethod().getResultValue();

            for(int i = 0; i < Config.COMPUTATION_NUMBER; i++) {
                double newValue = new SteepestDescentMethod().getResultValue();
                if(newValue < minValue) minValue = newValue;
                System.out.println(String.format("Iteration: %d, found value: %f", i, newValue));
            }
            System.out.println(minValue);
        });

    }
}
