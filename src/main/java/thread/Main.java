package thread;

import common.CheckedSupplier;
import common.Config;
import common.SteepestDescentMethod;
import common.TimeMetric;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static common.CheckedSupplierUtil.handleException;

public class Main {

    public static void  main(String... args) {
        //Empirically proved : threads work best, when fitted to number of available processors
        int availableProcessors = Runtime.getRuntime().availableProcessors();
        ExecutorService executorService = Executors.newFixedThreadPool(availableProcessors);

        CompletionService<Double> completionService =
                new ExecutorCompletionService<>(executorService);


        TimeMetric.runTimed(() -> {
            final List<Double> results = new ArrayList<>();

            for(int i = 0; i < Config.COMPUTATION_NUMBER; i++) {
                completionService.submit(() -> new SteepestDescentMethod().getResultValue());
            }

            int received = 0;

            while (received < Config.COMPUTATION_NUMBER) {
                Future<Double> resultFuture = handleException(completionService::take);
                Double result = handleException(resultFuture::get);
                results.add(result);
                received++;
            }

            System.out.println(results.stream().min(Double::compareTo));
        });

        executorService.shutdown();
    }

}
