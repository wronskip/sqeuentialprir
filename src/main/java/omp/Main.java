package omp;


import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ThreadLocalRandom;

import static java.lang.Math.*;
import static java.lang.String.format;
import static java.util.concurrent.TimeUnit.NANOSECONDS;

public class Main {

    public static void main(String... args) {
        TimeMetric.runTimed(() -> {
            List<Double> results = new CopyOnWriteArrayList<>();

            // omp parallel for
            for (int i = 0; i < 20000; i++) {
                results.add(new SteepestDescentMethod().getResultValue());
            }

            System.out.println(results.stream().min(Double::compareTo));

        });
        }

}

class Point {

    private static final double MIN = -10d;
    private static final double MAX = 10d;

    private final double x;
    private final double y;

    Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    static Point createRandomPoint() {
        return new Point(getRandomValue(), getRandomValue());
    }

    double getX() {
        return x;
    }

    double getY() {
        return y;
    }

    private static Double getRandomValue() {
        return ThreadLocalRandom.current().nextDouble(MIN, MAX);
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}

class Function {

    private final Double CONSTANT = 0.01D;

    private final Point point;

    Function(Point point) {
        this.point = point;
    }

    double getFunctionValue() {
        return getFunction().apply(point);
    }

    double getXGradientValue() {
        return getXGradientFunction().apply(point);
    }

    double getYGradientValue() {
        return getYGradientFunction().apply(point);
    }

    Point getNextGradientPoint(double epsilon) {
        return new Point(point.getX() - epsilon * getXGradientValue(), point.getY() - epsilon * getYGradientValue());
    }

    private java.util.function.Function<Point, Double> getFunction() {
        return (Point point) ->
                cos( 20 * point.getX()) + cos( 20 * point.getY() ) + CONSTANT * (pow(point.getX(), 2) + pow(point.getY(), 2));
    }

    private java.util.function.Function<Point, Double> getXGradientFunction() {
        return (Point point) -> 0.02 * point.getX() - 20 * sin(20 * point.getX());
    }

    private java.util.function.Function<Point, Double> getYGradientFunction() {
        return (Point point) -> 0.02 * point.getY() - 20 * sin(20 * point.getY());
    }
}

class SteepestDescentMethod {

    private static final int MAX_ITERATION_STEP = 100000;
    private static final double STOP_VALUE = 0.001D;

    private final Point result;

    private double epsilon = 0.01D;

    public SteepestDescentMethod() {
        this.result = computeResult();
    }

    public double getResultValue() {
        return new Function(getResult()).getFunctionValue();
    }

    public Point getResult() {
        return result;
    }

    private Point computeResult() {
        Point previousPoint;
        Point currentPoint = Point.createRandomPoint();


        for(int iterationStep = 0; iterationStep < MAX_ITERATION_STEP; iterationStep++){
            previousPoint = currentPoint;
            Function function = new Function(currentPoint);

            currentPoint = function.getNextGradientPoint(epsilon);

            double previousPointValue = new Function(previousPoint).getFunctionValue();
            double currentPointValue = function.getFunctionValue();

            if(currentPointValue < STOP_VALUE || currentPointValue - previousPointValue < epsilon) {
                break;
            }

            recalculateEpsilon(iterationStep);
        }

        return currentPoint;
    }


    private void recalculateEpsilon(int iterationStep) {
        if (iterationStep % 100 == 0) {
            epsilon *= 0.5;
        }
    }
}


class TimeMetric {

    private TimeMetric() {
        throw new UnsupportedOperationException("This class can not be instantiated!");
    }

    public static void runTimed(Runnable runnable) {
        System.out.println("Starting program");
        long startTime = System.nanoTime();
        runnable.run();
        long endTime = System.nanoTime();
        long executionTime = endTime - startTime;

        System.out.println(
                format(
                        "Execution took: %d ms == %d s",
                        NANOSECONDS.toMillis(executionTime),
                        NANOSECONDS.toSeconds(executionTime)
                )
        );
    }
}