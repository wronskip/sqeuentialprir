package ws.blocking.spark;

import com.mashape.unirest.http.Unirest;
import common.Config;
import common.SteepestDescentMethod;
import common.TimeMetric;

import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.IntStream;

import static common.CheckedSupplierUtil.handleException;
import static java.lang.Double.parseDouble;
import static java.util.stream.Collectors.toList;
import static spark.Spark.get;

public class Main {

    private static final String path = "/steepest-descent-method";

    private static final String fullPath = "http://localhost:4567/steepest-descent-method";

    public static void main(String... args) {
        int availableProcessors = Runtime.getRuntime().availableProcessors();
        final ForkJoinPool forkJoinPool = new ForkJoinPool(availableProcessors);


        get(path, (request, response) -> new SteepestDescentMethod().getResultValue());

        // We run parallel stream inside join pool to be sure
        // that it uses threads number == available processors
        forkJoinPool.submit(() ->
                TimeMetric.runTimed(() -> {
                    final List<Double> results = IntStream
                            .range(0, Config.COMPUTATION_NUMBER)
                            .parallel()
                            .mapToObj(i -> computeWithRestRequest())
                            .collect(toList());

                    System.out.println(results.stream().min(Double::compareTo));
                })
        );
    }

    static double computeWithRestRequest() {
        String response = handleException(() -> Unirest.get(fullPath).asObject(String.class).getBody());
        return parseDouble(response);
    }

}
