package ws.nonblocking.ratpack;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import common.Config;
import common.SteepestDescentMethod;
import common.TimeMetric;
import ratpack.server.RatpackServer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.Future;

import static common.CheckedSupplierUtil.handleException;
import static java.lang.Double.parseDouble;
import static java.lang.String.format;
import static java.util.stream.Collectors.toList;

public class Main {

    private static final String path = "steepest-descent-method";

    private static final String fullPath = "http://localhost:%d/" + path;

    public static void main(String... args) throws Exception {
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        final List<Integer> ports = initPorts(availableProcessors);

        // We create servers fitted to available processors
        // because Ratpack is based on single thread event-loop
        ports.forEach(Main::createServer);
        final ForkJoinPool forkJoinPool = new ForkJoinPool(availableProcessors);

        // We run parallel stream inside join pool to be sure
        // that it uses threads number == available processors
        forkJoinPool.submit(() -> {
                    TimeMetric.runTimed(() -> {
                        final List<Double> results = ports
                                .stream()
                                .parallel()
                                .map(port -> computeWithRestRequest(port, availableProcessors))
                                .flatMap(Collection::stream)
                                .collect(toList());

                        System.out.println(results.stream().min(Double::compareTo));
                    });
                    System.exit(1);
                }
        );
    }

    private static List<Integer> initPorts(final int availableProcessors) {
        final List<Integer> ports = new ArrayList<>();

        for (int port = 5050; port < 5050 + availableProcessors; port++) {
            ports.add(port);
        }

        return ports;
    }

    private static void createServer(int port) {
        handleException(() ->
                RatpackServer.start(server -> {
                            server.serverConfig(config -> config.port(port));
                            server.handlers(chain -> chain.get(path, context -> {
                                Double result = new SteepestDescentMethod().getResultValue();
                                context.getResponse().send(result.toString());
                            }));
                        }
                )
        );
    }

    static List<Double> computeWithRestRequest(int port, int availableProcessors) {
        final int batchSize = Config.COMPUTATION_NUMBER / availableProcessors;

        //We create single thread executor to send async request and then collect them
        ExecutorService service = Executors.newFixedThreadPool(1);
        CompletionService<String> completionService =
                new ExecutorCompletionService<>(service);

        //We batch requests for every available server
        for (int i = 0; i < batchSize; i++) {
            completionService.submit(
                    () -> handleException(
                            () -> Unirest.get(format(fullPath, port)).asObject(String.class).getBody()
                    )
            );
        }

        List<Double> results = new ArrayList<>();
        int received = 0;

        while (received < batchSize) {
            Future<String> future = handleException(completionService::take);
            String response = handleException(future::get);
            results.add(parseDouble(response));
            received++;
        }

        return results;
    }
}
