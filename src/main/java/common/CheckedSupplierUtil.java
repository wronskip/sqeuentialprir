package common;

public class CheckedSupplierUtil {

    public CheckedSupplierUtil() {
        throw new UnsupportedOperationException("This class can not be instantiated!");
    }

    public static <V, E extends Exception> V handleException(CheckedSupplier<V, E> supplier) {
        try {
            return supplier.get();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
            return null;
        }
    }
}
