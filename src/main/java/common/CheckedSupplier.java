package common;

@FunctionalInterface
public interface CheckedSupplier <V, E extends Exception>{

    V get() throws Exception;
}
