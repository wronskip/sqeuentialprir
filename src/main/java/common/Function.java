package common;

import static java.lang.Math.cos;
import static java.lang.Math.pow;
import static java.lang.Math.sin;

class Function {

    private final Double CONSTANT = 0.01D;

    private final Point point;

    Function(Point point) {
        this.point = point;
    }

    double getFunctionValue() {
        return getFunction().apply(point);
    }

    double getXGradientValue() {
        return getXGradientFunction().apply(point);
    }

    double getYGradientValue() {
        return getYGradientFunction().apply(point);
    }

    Point getNextGradientPoint(double epsilon) {
        return new Point(point.getX() - epsilon * getXGradientValue(), point.getY() - epsilon * getYGradientValue());
    }

    private java.util.function.Function<Point, Double> getFunction() {
        return (Point point) ->
            cos( 20 * point.getX()) + cos( 20 * point.getY() ) + CONSTANT * (pow(point.getX(), 2) + pow(point.getY(), 2));
    }

    private java.util.function.Function<Point, Double> getXGradientFunction() {
        return (Point point) -> 0.02 * point.getX() - 20 * sin(20 * point.getX());
    }

    private java.util.function.Function<Point, Double> getYGradientFunction() {
        return (Point point) -> 0.02 * point.getY() - 20 * sin(20 * point.getY());
     }
}
