package common;

public class SteepestDescentMethod {

    private static final int MAX_ITERATION_STEP = 100000;
    private static final double STOP_VALUE = 0.001D;

    private final Point result;

    private double epsilon = 0.01D;

    public SteepestDescentMethod() {
        this.result = computeResult();
    }

    public double getResultValue() {
        return new Function(getResult()).getFunctionValue();
    }

    public Point getResult() {
        return result;
    }

    private Point computeResult() {
        Point previousPoint;
        Point currentPoint = Point.createRandomPoint();


        for(int iterationStep = 0; iterationStep < MAX_ITERATION_STEP; iterationStep++){
            previousPoint = currentPoint;
            Function function = new Function(currentPoint);

            currentPoint = function.getNextGradientPoint(epsilon);

            double previousPointValue = new Function(previousPoint).getFunctionValue();
            double currentPointValue = function.getFunctionValue();

            if(currentPointValue < STOP_VALUE || currentPointValue - previousPointValue < epsilon) {
                break;
            }

            recalculateEpsilon(iterationStep);
        }

        return currentPoint;
    }


    private void recalculateEpsilon(int iterationStep) {
        if (iterationStep % 100 == 0) {
            epsilon *= 0.5;
           }
    }
}
