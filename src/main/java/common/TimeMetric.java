package common;

import static java.lang.String.format;
import static java.util.concurrent.TimeUnit.NANOSECONDS;

public class TimeMetric {

    private TimeMetric() {
        throw new UnsupportedOperationException("This class can not be instantiated!");
    }

    public static void runTimed(Runnable runnable) {
        long startTime = System.nanoTime();
        runnable.run();
        long endTime = System.nanoTime();
        long executionTime = endTime - startTime;

        System.out.println(
                format(
                        "Execution took: %d ms == %d s",
                        NANOSECONDS.toMillis(executionTime),
                        NANOSECONDS.toSeconds(executionTime)
                )
        );
    }
}
