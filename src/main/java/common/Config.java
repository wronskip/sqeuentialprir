package common;

public class Config {

    private Config() {
        throw new UnsupportedOperationException("This class can not be instantiated!");
    }

    public static final int COMPUTATION_NUMBER = 2000000;
}
