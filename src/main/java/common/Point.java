package common;

import java.util.concurrent.ThreadLocalRandom;

class Point {

    private static final double MIN = -10d;
    private static final double MAX = 10d;

    private final double x;
    private final double y;

    Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    static Point createRandomPoint() {
        return new Point(getRandomValue(), getRandomValue());
    }

    double getX() {
        return x;
    }

    double getY() {
        return y;
    }

    private static Double getRandomValue() {
        return ThreadLocalRandom.current().nextDouble(MIN, MAX);
    }

    @Override
    public String toString() {
        return "common.Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
